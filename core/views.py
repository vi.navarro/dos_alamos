from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from .models import *
from .forms import *
from django.contrib import messages
from django.core.paginator import Paginator
from django.http import Http404, HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.shortcuts import render
from .forms import *
from .utils import *

from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your views here.

def index(request):
    return render(request, 'core/index.html')

def login(request):
    data = {
        'form':LoginForm()
    }
    if request.method == 'POST':
        formulario = LoginForm(data=request.POST)
        if formulario.is_valid():
            Usuario = authenticate(username=formulario.cleaned_data["email"],password=formulario.cleaned_data["password1"])
            auth_login(request,Usuario)
            messages.success(request, "Logueo exitoso")
            return redirect(to="index")
        data['from'] = formulario
    return render(request, 'registration/login.html', data)

def especialidades(request):
    return render(request, 'core/especialidades.html')

def sucursales(request):
    return render(request, 'core/sucursales.html')

def servicios(request):
    return render(request, 'core/servicios.html')



def register(request):
    data = {
        'form':RegistroForms()
    }
    if request.method == 'POST':
        formulario = RegistroForms(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            Usuario = authenticate(username=formulario.cleaned_data["email"],password=formulario.cleaned_data["password1"])
            auth_login(request,Usuario)
            messages.success(request, "Registro exitoso")
            return redirect(to="index")
        data['from'] = formulario
    return render(request, 'registration/register.html', data)




def agregar_psemanal(request):

    data= {
        'form': PagoSemanalForm()
    }

    if request.method == 'POST':
        formulario = PagoSemanalForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Guardado Correctamente"
        else:
            data["form"] = formulario
    return render(request, 'core/pago.html' , data)

def agregar_pmensual(request):

    data= {
        'form': PagoMensualForm()
    }

    if request.method == 'POST':
        formulario = PagoMensualForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Guardado Correctamente"
        else:
            data["form"] = formulario
    return render(request, 'core/pago_mensual.html' , data)



def pago(request):
    return render(request, 'core/pago.html')



def agregarHorarios(request):
    data= {
        'form': horarioMedicoForms(),
    }
    if request.method == 'POST':
        formulario = horarioMedicoForms(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Guardado Correctamente"
        else:
            data["form"] = formulario
    return render(request, 'core/horarioMedico.html', data)
    
def modificarHorarios(request, id):
    product = get_object_or_404(horarioMedico, id=id)
    data = {
        'form': horarioMedicoForms(instance=product)
    }
    if request.method == 'POST':
        formulario = horarioMedicoForms(data=request.POST,instance=product, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            messages.success(request, " Horario modificado correctamente ")
            return redirect(to="listHorarios")
        else:
            data["form"] = formulario

def reserva(request):
    return render(request, 'core/user/reserva.html')

def lista(request):
    comprobante = comprobantePagoSemanal.objects.all()
    data = {
        'comprobante' : comprobante
    }
    return render(request, 'core/listar_pagos.html', data)


def lista_pmensual(request):
    comprobante2 = ComprobantePagoMedico.objects.all()
    data = {
        'comprobante2' : comprobante2
    }
    return render(request, 'core/lista_pagomensual.html', data)



def listarHorarios(request):
    horario = horarioMedico.objects.all()
    data = {
        'horario' : horario
    }
    return render(request, 'core/listHorarios.html', data)

#vista para la boleta
def BoletaListView(self, *args, **kwargs):
    comprobante = comprobantePagoSemanal.objects.all()
    data = {
        'comprobante' : comprobante
    }
    pdf = render_to_pdf('core/boletas.html', data)
    return HttpResponse(pdf, content_type='application/pdf')