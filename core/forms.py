from django import forms
from .models import *
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models import fields
from django.contrib.admin import widgets

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegistroForms(UserCreationForm):

    class Meta:
        model = Usuario
        fields = ["email", "run", "nombres","apellidos","password1","password2"]

class LoginForm(AuthenticationForm):
    class Meta:
        model = Usuario
        fields = ["email", "password"]
        
class DateInput(forms.DateInput):
  input_type = 'date'

class TimeInput(forms.TimeInput):
  input_type = 'time'
  
class PagoSemanalForm(forms.ModelForm):
    class Meta:
        model = comprobantePagoSemanal
        fields = '__all__'
        widgets = {
          'fecha_comprobante' : DateInput(),
        }

class PagoMensualForm(forms.ModelForm):
    class Meta:
        model = ComprobantePagoMedico
        fields = '__all__'

class horarioMedicoForms(forms.ModelForm):
    class Meta:
      model = horarioMedico
      fields = ['rut_medico','fecha','horaInicio','horaFin']
      widgets = {
        'fecha' : DateInput(),
        'horaInicio' : TimeInput(),
        'horaFin' : TimeInput(),
        }
