from django.db import models
from django.db.models.base import ModelState
from django.db.models.expressions import Case
from django.db.models.fields import BooleanField, CharField, NullBooleanField
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UsuarioManager(BaseUserManager):
    def create_user(self,email,run,nombres,apellidos,password):
        if not email:
            raise ValueError('El usuario debe tener un email')

        usuario = self.model(
            email =email,
            run = run,
            nombres = nombres,
            apellidos = apellidos,
            password = password
        )

        usuario.set_password(password)
        usuario.save()
        return usuario
    
    def create_superuser(self,email,run,nombres,apellidos,password):
        usuario = self.create_user(
            email,
            run= run,
            nombres = nombres,
            apellidos = apellidos,
            password = password
        )
        usuario.usuario_administrador = True
        usuario.save()
        return usuario

class Usuario(AbstractBaseUser):
    email = models.CharField('Email',unique=True,max_length=254,null=False,blank=False)
    run = models.IntegerField('Run', unique=True,primary_key=True,null=False,blank=False)
    nombres = models.CharField('Nombres',max_length=254,null=False,blank=False)
    apellidos = models.CharField('Apellidos',max_length=254,null=False,blank=False)
    usuario_activo = models.BooleanField(default=True)
    usuario_administrador = models.BooleanField(default=False)
    objects = UsuarioManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nombres','apellidos','run','password']

    def __str__(self):
        return str(self.run)

    def has_perm(self,perm,obj = None):
        return True

    def has_module_perms(self,core):
        return True
    
    @property
    def is_staff(self):
        return self.usuario_administrador

class ComprobantePagoMedico(models.Model):
    monto = models.FloatField()
    rut_medico = models.ForeignKey('Usuario',models.DO_NOTHING, db_column='run', limit_choices_to={'usuario_administrador': True})
    comprobantePagoSemanal = models.ForeignKey('comprobantePagoSemanal', on_delete=models.PROTECT)

    def __str__(self):
        return str(self.rut_medico)

class comprobantePagoSemanal(models.Model):
    monto_total = models.FloatField()
    fecha_comprobante = models.DateField()
    rut_medico = models.ForeignKey('Usuario',models.DO_NOTHING, db_column='run', limit_choices_to={'usuario_administrador': True})

    def __str__(self):
        return str(self.rut_medico)

class datosMedico(models.Model):
    rut_medico = models.CharField(max_length=200)
    nom_medico = models.CharField(max_length=200)
    ape_medico = models.CharField(max_length=200)
    telefono = models.IntegerField()
    especialidad = models.CharField(max_length=100)

    def __str__(self):
        return self.nom_medico

class datosPaciente(models.Model):
    rut_paciente = models.CharField(max_length=20)
    nom_paciente = models.CharField(max_length=50)
    ape_paciente = models.CharField(max_length=50)
    telefono = models.IntegerField()
    causa_consulta = models.CharField(max_length=500)

    def __str__(self):
        return self.nom_medico

class horarioMedico(models.Model):
    fecha = models.DateField()
    horaInicio = models.TimeField()
    horaFin = models.TimeField()
    rut_medico = models.CharField(max_length=10)
    rut_paciente = models.CharField(max_length=10, null=True, blank=True)
    disponible = models.BooleanField(default=True)

    def __str__(self):
        return str(self.fecha)