from django.urls import path 
from .views import *


urlpatterns = [
    path('', index, name='index'),
    path('especialidades/', especialidades, name="especialidades"),
    path('sucursales/', sucursales, name="sucursales"),
    path('servicios/', servicios, name="servicios"),
    path('register/', register, name="register"),
    path('accounts/login/', login, name="login"),
    path('pago/', agregar_psemanal, name="pago"),
    path('lista/', lista, name="lista"),
    path('horarioMedico/', agregarHorarios, name="horarioMedico"),
    path('listHorarios/', listarHorarios, name="listHorarios"),
    path('modificarHorarios/<id>/', modificarHorarios, name="modificarHorarios"),
    path('reserva/', reserva, name="reserva"),
    path('listarHorarios/', listarHorarios, name="listarHorarios"),
    path('pago-mensual/', agregar_pmensual, name="pagomensual"),
    path('lista-mensual/', lista_pmensual, name="listamensual"),
    path('boleta/<id>', BoletaListView, name="boleta"),
]